package com.epam.course.renchka.task2.controller;

import com.epam.course.renchka.task2.model.Model;

public class Controller {

  private Model model;

  public Controller(Model model) {
    this.model = model;
  }

  public String executeCommand(String command, String argument) {
    return model.executeCommand(command, argument);
  }
}
