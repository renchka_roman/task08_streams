package com.epam.course.renchka.task2.model;

public enum Commands {
  LAMBDA,
  REFERENCE,
  ANONYMOUS,
  CLASS
}
