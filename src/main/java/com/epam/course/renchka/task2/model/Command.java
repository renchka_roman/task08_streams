package com.epam.course.renchka.task2.model;

public interface Command {

  String execute(String argument);
}
