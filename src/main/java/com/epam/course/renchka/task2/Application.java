package com.epam.course.renchka.task2;

import com.epam.course.renchka.task2.controller.Controller;
import com.epam.course.renchka.task2.model.Model;
import com.epam.course.renchka.task2.view.View;
import java.io.IOException;

public class Application {

  public static void main(String[] args) throws IOException {
    Model model = new Model();
    Controller controller = new Controller(model);
    View view = new View(controller);
    view.start();
  }
}
