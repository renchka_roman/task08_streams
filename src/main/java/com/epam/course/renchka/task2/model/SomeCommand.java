package com.epam.course.renchka.task2.model;

public class SomeCommand implements Command {

  @Override
  public String execute(String argument) {
    return "Return \"" + argument + "\" from object of class";
  }
}
