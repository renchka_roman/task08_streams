package com.epam.course.renchka.task1;

public interface VariablesOperation {

  int execute(int v1, int v2, int v3);
}
