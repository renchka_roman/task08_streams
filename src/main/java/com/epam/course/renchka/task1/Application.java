package com.epam.course.renchka.task1;

import java.util.stream.IntStream;

public class Application {

  public static void main(String[] args) {
    System.out.println(getMax(200, 100, 199));
    System.out.println(getAverage(200, 100, 199));
  }

  public static int getMax(int var1, int var2, int var3) {
    VariablesOperation operation = (v1, v2, v3) -> IntStream.of(v1, v2, v3)
        .max().getAsInt();
    return operation.execute(var1, var2, var3);
  }

  public static int getAverage(int var1, int var2, int var3) {
    VariablesOperation operation = (v1, v2, v3) -> {
      double result = IntStream.of(v1, v2, v3).average().getAsDouble();
      return (int) result;
    };
    return operation.execute(var1, var2, var3);
  }
}
