package com.epam.course.renchka.task4.view;

@FunctionalInterface
public interface Printable {

  void print();
}
