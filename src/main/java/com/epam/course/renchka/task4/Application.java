package com.epam.course.renchka.task4;

import com.epam.course.renchka.task4.controller.Controller;
import com.epam.course.renchka.task4.model.Domain;
import com.epam.course.renchka.task4.model.Model;
import com.epam.course.renchka.task4.view.View;
import java.io.IOException;

public class Application {

  public static void main(String[] args) throws IOException {
    Model model = new Model(new Domain());
    Controller controller = new Controller(model);
    View view = new View(controller);
    view.start();
  }
}
